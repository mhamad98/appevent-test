import React from 'react';
import { NavLink } from 'react-router-dom';
import Router from './Router';

import Busqet from './features/cart/Busqet'

const Navigation = (props) =>
  <nav className='Navigation'>
    <ul className='Navigation-bar'>
      <li><NavLink to='/'>Catalog</NavLink></li>
      <li><NavLink to='/cart'>Cart (<Busqet />)</NavLink></li>
    </ul>
  </nav>

function App() {
  return (
    <div className='page-container'>
      <Navigation products={JSON.parse(localStorage.getItem("data1")).items}/>
      <Router />
    </div>
  );
}



export default App;
