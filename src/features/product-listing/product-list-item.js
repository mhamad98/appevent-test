import React from 'react';
import AddButton from './add-button'
import RemoveButton from './remove-button'
import { NavLink } from 'react-router-dom'; 

export default function ProductListItem(props) {
    return (
        <div className="List-item">
            <div className="List-item__name">{props.product.name}</div>
            <img
                className="List-item__image"
                height={100}
                title={props.product.name}
                src={props.product.image}
            />
            <div className="List-item__price">{props.product.price}руб</div>

            <AddButton
                cartItem={props.cartItem}
                product={props.product}
                addToCart={props.addToCart}
            />
            {
                props.cartItem
                    ? <RemoveButton
                        cartItem={props.cartItem}
                        product={props.product}
                        removeFromCart={props.removeFromCart}
                    />
                    : null
            }
            {
                props.cartItem
                    ? <NavLink to='/cart'>Оформить заказ</NavLink>
                    : null
            }


        </div>
    )
}
