import React from 'react'
import { connect } from 'react-redux'



function Cart(props) {
    let totalItem = 0;
    let totalPrice = 0;
    return (
        <div>
            {
                props.cart[0]
                    ? <div className='Cart'>
                        <table className='Cart-table' border='1'>
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Add</th>
                                    <th>remove</th>
                                    <th>removeAll</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    props.cart.map(item => <tr>
                                        <td>{item.name}</td>
                                        <td>{item.quantity}</td>
                                        <td>{item.quantity * item.price}руб</td>
                                        <td>
                                            <button
                                                onClick={(e) => props.addToCart(item)}
                                            >+</button>
                                        </td>
                                        <td>
                                            <button
                                                onClick={(e) => props.removeFromCart(item)}
                                            >-</button>
                                        </td>
                                        <td>
                                            <button
                                                onClick={(e) => props.removeAllFromCart(item)}
                                            >Clear cart</button>
                                        </td>

                                    </tr>)
                                }

                            </tbody>
                        </table>
                        <div className='Cart-total'>
                            <div className='Cart-total__title'>Total:</div>
                            {
                                props.cart.map(item => {
                                    totalPrice = totalPrice + (item.price * item.quantity)
                                })
                            }
                            <div className='Cart-total__price'>{totalPrice}</div>

                        </div>
                    </div>
                    : <div>Cart is empty</div>
            }

        </div>

    )

}

function mapStateToProps(state) {
    return {
        cart: state.cart
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addToCart: (item) => {
            dispatch({ type: 'ADD', payload: item })
        },
        removeFromCart: (item) => {
            dispatch({ type: 'REMOVE', payload: item })
        },
        removeAllFromCart: (item) => {
            dispatch({ type: 'REMOVE_ALL', payload: item })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
