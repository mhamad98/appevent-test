import React from 'react';
import ProductListing from '../features/product-listing';

let data1;

async function FetchProducts() { 
    const url = "https://appevent.ru/dev/task1/catalog";
    const response = await fetch(url);
    data1 = await response.json();
    let products= JSON.stringify(data1);
    localStorage.setItem("data2",products)
}

FetchProducts()

export default function Homepage(props) {
    
    return (
        <div>
            <h2>Catalog</h2>
            <div>{}</div>
                <ProductListing products={JSON.parse(localStorage.getItem("data2")).items}/>
        </div>
    )
}